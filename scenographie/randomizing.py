import bpy
import random
from mathutils import *

list_transformations = ['lX', 'lY', 'rZ']

bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_MASS')

for obj in bpy.context.selected_objects:
    choix = random.choice(list_transformations)
    if choix == 'lX':
        obj.location.x += random.randint(-5, 5)/1000.
    elif choix == 'lY':
        obj.location.y += random.randint(-5, 5)/1000.
    elif choix == 'rZ':
        obj.rotation_euler.z += random.randint(-1, 1)/20.