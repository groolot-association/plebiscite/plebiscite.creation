#
#  Filename : plebiscite_freestyle.py
#  Author   : Gregory DAVID
#  Date     : 10/05/2013
#  Purpose  : Simulation du trace a la Sandra GUILLEN
#
#############################################################################  
#
#  Copyright (C) : Please refer to the COPYRIGHT file distributed 
#  with this source distribution. 
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
#############################################################################

from freestyle import *
from logical_operators import *
from PredicatesU0D import *
from PredicatesU1D import *
from PredicatesB1D import *
from Functions0D import *
from shaders import *

import bpy
import math
import mathutils

# Source code from Tamito Kajiyama 26/07/2010 GPLv2
# modified by Gregory DAVID 03/06/2013 GPLv3
def my_iter_distance_from_camera(stroke, range_min, range_max):
    normfac = range_max - range_min # normalization factor
    it = stroke.stroke_vertices_begin()
    while not it.is_end:
        p = it.object.point_3d # in the camera coordinate
        distance = p.length
        if distance < range_min:
            t = 0.0
        elif distance > range_max:
            t = 1.0
        else:
            t = (distance - range_min) / normfac
        yield it, t
        it.increment()

def my_iter_material_specular_color(stroke):
    func = CurveMaterialF0D()
    it = stroke.stroke_vertices_begin()
    while not it.is_end:
        material = func(Interface0DIterator(it))
        color = material.specular[0:3]
        yield it, color
        it.increment()

# End of external source code, modified

# Distance from Camera modifier
class myColorDistanceFromCameraShader(StrokeShader):
    def __init__(self,
        range_min = bpy.context.scene.camera.data.plebiscite_freestyle_distance_from_camera_range_min,
        range_max = bpy.context.scene.camera.data.plebiscite_freestyle_distance_from_camera_range_max,
        color_near = bpy.context.scene.camera.data.plebiscite_freestyle_distance_from_camera_color_near,
        color_far = bpy.context.scene.camera.data.plebiscite_freestyle_distance_from_camera_color_far
        ):
        StrokeShader.__init__(self)
        self.__range_min = range_min
        self.__range_max = range_max
        self.__color_near = mathutils.Vector(color_near).xyz
        self.__alpha_near = mathutils.Vector(color_near).w
        self.__color_far = mathutils.Vector(color_far).xyz
        self.__alpha_far = mathutils.Vector(color_far).w
    def shade(self, stroke):
        distance = self.__color_far - self.__color_near
        adistance = self.__alpha_far - self.__alpha_near
        for it, t in my_iter_distance_from_camera(stroke, self.__range_min, self.__range_max):
            sv = it.object
            sv.attribute.color = self.__color_near + t * distance
            sv.attribute.alpha = self.__alpha_near + t * adistance

class myThicknessDistanceFromCameraShader(StrokeShader):
    def __init__(self,
        range_min = bpy.context.scene.camera.data.plebiscite_freestyle_distance_from_camera_range_min,
        range_max = bpy.context.scene.camera.data.plebiscite_freestyle_distance_from_camera_range_max,
        thickness_near = bpy.context.scene.camera.data.plebiscite_freestyle_distance_from_camera_thickness_near,
        thickness_far = bpy.context.scene.camera.data.plebiscite_freestyle_distance_from_camera_thickness_far
        ):
        StrokeShader.__init__(self)
        self.__range_min = range_min
        self.__range_max = range_max
        self.__thickness_near = thickness_near
        self.__thickness_far = thickness_far
    def shade(self, stroke):
        distance = self.__thickness_far - self.__thickness_near
        for it, t in my_iter_distance_from_camera(stroke, self.__range_min, self.__range_max):
            sv = it.object
            sv.attribute.thickness = sv.attribute.thickness * (self.__thickness_near + distance * t)


# Material modifiers
class mySpecularColorMaterialShader(StrokeShader):
    def __init__(self):
        StrokeShader.__init__(self)
    def shade(self, stroke):
        for it, color in my_iter_material_specular_color(stroke):
            sv = it.object
            sv.attribute.color = color


#############################
### Shader's construction ###
#############################

Operators.select(QuantitativeInvisibilityUP1D(0))
Operators.bidirectional_chain(ChainSilhouetteIterator(), NotUP1D(QuantitativeInvisibilityUP1D(0)))

func = pyInverseCurvature2DAngleF0D()
Operators.recursive_split(func, pyParameterUP0D(0.3,0.8), NotUP1D(pyHigherNumberOfTurnsUP1D(3, 0.5)), 2)
Operators.select(pyHigherLengthUP1D(0))
Operators.sort(pyLengthBP1D())
shaders_list = [
    pySamplingShader(3),
    BezierCurveShader(10),
    SamplingShader(50),
    ConstrainedIncreasingThicknessShader(1.0, 6.5, 0.1),
    ConstantColorShader(1.0, 1.0, 1.0, 1.0),
    myThicknessDistanceFromCameraShader(),
    myColorDistanceFromCameraShader(),
    mySpecularColorMaterialShader(),
]
Operators.create(TrueUP1D(), shaders_list)