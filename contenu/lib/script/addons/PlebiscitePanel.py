# ***** BEGIN GPL LICENSE BLOCK *****
#
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ***** END GPL LICENCE BLOCK *****


bl_info = {
    "name": "Plebiscite pipeline parameters",
    "author": "Gregory David (aka groolot)",
    "version": (0, 1, 0),
    "blender": (2, 67, 0),
    "location": "Properties panel > Render > Plebiscite parameters",
    "description": "Parameters for the Plebiscite render pipeline",
    "warning": "Do need the plebiscite_freestyle.py module to be useful.",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Render"}

import bpy

def init_properties():
    scene = bpy.types.Scene
    wm = bpy.types.WindowManager

    scene.plebiscite_freestyle_distance_from_camera_range_min = bpy.props.IntProperty(
        name = "Distance min",
        description = "Distance from camera minimum range",
        default = 0,
        min = 0,
        max = 100000)
    scene.plebiscite_freestyle_distance_from_camera_range_max = bpy.props.IntProperty(
        name = "Distance max",
        description = "Distance from camera maximum range",
        default = 100,
        min = 0,
        max = 100000)
    scene.plebiscite_freestyle_distance_from_camera_thickness_near = bpy.props.FloatProperty(
        name = "Near thickness factor",
        description = "Distance from camera near thickness factor",
        default = 1,
        min = 0,
        max = 1)
    scene.plebiscite_freestyle_distance_from_camera_thickness_far = bpy.props.FloatProperty(
        name = "Far thickness factor",
        description = "Distance from camera far thickness factor",
        default = 1,
        min = 0,
        max = 1)
    scene.plebiscite_freestyle_distance_from_camera_color_near = bpy.props.FloatVectorProperty(
        name = "Near color",
        description = "Distance from camera near color",
        default = (1.0, 1.0, 1.0, 1.0),
        min = 0,
        max = 1,
        subtype = 'COLOR',
        size = 4)
    scene.plebiscite_freestyle_distance_from_camera_color_far = bpy.props.FloatVectorProperty(
        name = "Far color",
        description = "Distance from camera far color",
        default = (0.0, 0.0, 0.0, 1.0),
        min = 0,
        max = 1,
        subtype = 'COLOR',
        size = 4)

# removal of properties when script is disabled
def clear_properties():
    props = (
        "plebiscite_freestyle_distance_from_camera_range_min",
        "plebiscite_freestyle_distance_from_camera_range_max",
        "plebiscite_freestyle_distance_from_camera_thickness_near",
        "plebiscite_freestyle_distance_from_camera_thickness_far",
        "plebiscite_freestyle_distance_from_camera_color_near",
        "plebiscite_freestyle_distance_from_camera_color_far",
    )
    wm = bpy.context.window_manager
    for p in props:
        if p in wm:
            del wm[p]

class PlebiscitePanel(bpy.types.Panel):
    """Creates a Panel in the RenderLayer properties window"""
    bl_label = "Plebiscite parameters"
    bl_idname = "RENDER_PT_Plebiscite"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "render"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        sc = context.scene
        wm = context.window_manager
        layout = self.layout

        row = layout.row()
        row.label(text = "Collection of Plebiscite's pipeline parameters", icon = 'WORLD_DATA')
        row = layout.row()
        row.prop(sc, "plebiscite_freestyle_distance_from_camera_range_min")
        row = layout.row()
        row.prop(sc, "plebiscite_freestyle_distance_from_camera_range_max")

        layout.separator()

        row = layout.row(align = True)
        row.prop(sc, "plebiscite_freestyle_distance_from_camera_thickness_near")
        row = layout.row(align = True)
        row.prop(sc, "plebiscite_freestyle_distance_from_camera_thickness_far")

        layout.separator()

        row = layout.row(align = True)
        row.prop(sc, "plebiscite_freestyle_distance_from_camera_color_near")
        row = layout.row(align = True)
        row.prop(sc, "plebiscite_freestyle_distance_from_camera_color_far")


def register():
    init_properties()
    bpy.utils.register_class(PlebiscitePanel)


def unregister():
    clear_properties()
    bpy.utils.unregister_class(PlebiscitePanel)


if __name__ == "__main__":
    register()
